\contentsline {section}{\numberline {1}Einf\"uhrung}{1}{section.1}%
\contentsline {subsection}{\numberline {1.1}Motivation}{1}{subsection.1.1}%
\contentsline {subsection}{\numberline {1.2}Zielstellung}{1}{subsection.1.2}%
\contentsline {subsection}{\numberline {1.3}Lösungsweg}{2}{subsection.1.3}%
\contentsline {section}{\numberline {2}Grundlagen}{3}{section.2}%
\contentsline {subsection}{\numberline {2.1}Arrays allgemein}{3}{subsection.2.1}%
\contentsline {subsubsection}{\numberline {2.1.1}Erstellung und Repräsentation}{3}{subsubsection.2.1.1}%
\contentsline {subsubsection}{\numberline {2.1.2}Funktionalitäten}{5}{subsubsection.2.1.2}%
\contentsline {subsection}{\numberline {2.2}Arrays in Java}{5}{subsection.2.2}%
\contentsline {subsubsection}{\numberline {2.2.1}Initialisierung}{5}{subsubsection.2.2.1}%
\contentsline {subsubsection}{\numberline {2.2.2}Generische Programmierbarkeit}{6}{subsubsection.2.2.2}%
\contentsline {subsubsection}{\numberline {2.2.3}Dimensionalität}{6}{subsubsection.2.2.3}%
\contentsline {subsubsection}{\numberline {2.2.4}Adressierung}{7}{subsubsection.2.2.4}%
\contentsline {subsubsection}{\numberline {2.2.5}Manipulation}{7}{subsubsection.2.2.5}%
\contentsline {subsubsection}{\numberline {2.2.6}Iteration}{7}{subsubsection.2.2.6}%
\contentsline {subsection}{\numberline {2.3}Typische Fehler in Verbindung mit Arrays}{8}{subsection.2.3}%
\contentsline {subsection}{\numberline {2.4}State of the art}{10}{subsection.2.4}%
\contentsline {section}{\numberline {3}Design}{12}{section.3}%
\contentsline {subsection}{\numberline {3.1}Array Erstellung und Repräsentation}{12}{subsection.3.1}%
\contentsline {subsubsection}{\numberline {3.1.1}Initialisierung}{12}{subsubsection.3.1.1}%
\contentsline {subsubsection}{\numberline {3.1.2}Repräsentation}{13}{subsubsection.3.1.2}%
\contentsline {subsection}{\numberline {3.2}Array Operationen}{14}{subsection.3.2}%
\contentsline {subsubsection}{\numberline {3.2.1}Adressierung}{15}{subsubsection.3.2.1}%
\contentsline {subsubsection}{\numberline {3.2.2}Manipulation}{15}{subsubsection.3.2.2}%
\contentsline {subsubsection}{\numberline {3.2.3}Iteration}{16}{subsubsection.3.2.3}%
\contentsline {subsection}{\numberline {3.3}Array Fehlerdarstellung}{18}{subsection.3.3}%
\contentsline {subsection}{\numberline {3.4}2-dimensionale Arrays}{20}{subsection.3.4}%
\contentsline {section}{\numberline {4}Implementierung}{22}{section.4}%
\contentsline {subsection}{\numberline {4.1}Setup}{22}{subsection.4.1}%
\contentsline {subsection}{\numberline {4.2}Dateien}{22}{subsection.4.2}%
\contentsline {subsection}{\numberline {4.3}Aufbau des Programms}{23}{subsection.4.3}%
\contentsline {subsubsection}{\numberline {4.3.1}Zustandsvariablen}{23}{subsubsection.4.3.1}%
\contentsline {subsubsection}{\numberline {4.3.2}Zustandsänderungen}{23}{subsubsection.4.3.2}%
\contentsline {subsection}{\numberline {4.4}Ausblick}{24}{subsection.4.4}%
\contentsline {section}{\numberline {5}Zusammenfassung}{26}{section.5}%
\contentsline {section}{Anhang}{27}{section*.3}%
\contentsline {section}{Literatur}{29}{section*.4}%
