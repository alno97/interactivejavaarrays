import { useState } from "react"

export interface MenuProps {
    arrayCallback(newArrayType: string): void;
    arrayType: string;
}

export default function Menu(props:MenuProps) {
    return(
        <nav className="nav">
            <button className={props.arrayType === "1d" ? "menuButton active" : "menuButton"} onClick={() => props.arrayCallback("1d")}>1-D Array</button>
            <button className={props.arrayType === "2d" ? "menuButton active" : "menuButton"} onClick={() => props.arrayCallback("2d")}>2-D Array</button>
        </nav>
    )
}