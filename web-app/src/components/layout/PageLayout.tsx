import { useCallback, useState } from "react";
import ArrayPage from "../../pages/array/ArrayPage";
import MultiArrayPage from "../../pages/multiArray/MultiArrayPage";
import Menu from "./Menu";

export default function PageLayout(props:any) {
    const [arrayType, setArrayType] = useState("1d");

    const arrayCallback = useCallback((newArrayType: string) => {
        setArrayType(newArrayType);
    },[])

    return (
        <div className="layout">
            <Menu arrayCallback={arrayCallback} arrayType={arrayType}/>
            {arrayType === "1d" ? 
                <ArrayPage multi={false} />
                :
                <MultiArrayPage />
            }
            <footer className="footer">
                <p>Fakultät IA, Graphische Datenverarbeitung</p>
                <p>Author: Alexander Nöbel</p>
                <p><a href="mailto:alexander.noebel@tu-ilmenau.de">alexander.noebel@tu-ilmenau.de</a></p>
            </footer>
        </div>
    )
}