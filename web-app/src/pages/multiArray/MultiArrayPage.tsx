import ArrayPage from "../array/ArrayPage";

export default function MultiArrayPage() {
    return (
        <ArrayPage multi={true}/>
    )
}