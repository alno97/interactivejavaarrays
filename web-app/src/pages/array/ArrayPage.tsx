import { useCallback, useEffect, useRef, useState } from "react";
import PageLayout from "../../components/layout/PageLayout";
import { Array, MultiArray } from "./Array";
import {Section, Function, Traversing} from "./ArrayComponents";

interface ArrayPageProps {
    multi: boolean;
}

function usePrevious(value:any) {
    const ref = useRef<any>();
    useEffect(() => {
        ref.current = value;
    });
    return ref.current;
}

export default function ArrayPage(props:ArrayPageProps) {
    const [array, setArray] = useState<Array<any>>([]);
    const [index, setIndex] = useState<number | undefined>();
    const [field, setField] = useState<number | undefined>();
    const prevIndex = usePrevious(index);
    const [errorA, setErrorA] = useState("");
    const [errorR, setErrorR] = useState("");
    const [errorW, setErrorW] = useState("");
    const [startIndex, setStartIndex] = useState<number | undefined>();
    const [endIndex, setEndIndex] = useState<number | undefined>();
    const [direction, setDirection] = useState(1);
    const [type, setType] = useState("int");
    const [accumulator, setAccumulator] = useState<any|undefined>();

    const onArrayChange = useCallback((input: string) => {
        if (!(input.slice(-";".length) === ";")){
            setErrorA("; erwartet");
            return;
        }
        var s = input.replace(/\s/g, "").slice(0,-1);
        var validate = /^{([0-9-],?)*}$/.test(s);
        if (type === "bool") {
            validate = /^{([true|false],?)*}$/.test(s);
        }
        if (type === "string") {
            validate = /^{("[a-z,0-9\s]*",?)*}$/.test(s);
        }
        if (!validate) {
            setErrorA("Invalid Array");
            return;
        }
        setErrorA("");
        try {
            const help = JSON.parse(s.replace("{", "[").replace("}", "]"));
            setArray(help);
        } catch (error) {
            setErrorA("Parsing Error");
        }
    }, [type])

    const onMultiArrayChange = useCallback((input: string) => {
        if (!(input.slice(-";".length) === ";")){
            setErrorA("; erwartet");
            return;
        }
        var s = input.replace(/\s/g, "").slice(0,-1);
        var validate = /{({([0-9],?)*},?)*}/.test(s);
        if (type === "bool") {
            validate = /{({([true|false],?)*},?)*}/.test(s);
        }
        if (type === "string") {
            validate = /{({("[0-9,a-z]*",?)*},?)*}/.test(s);
        }
        if (!validate) {
            setErrorA("Invalid Array");
            return;
        }
        setErrorA("");
        const help = JSON.parse(s.replaceAll("{", "[").replaceAll("}", "]"))
        setArray(help);
    }, [type])

    const onReadChange = useCallback((input:string) => {
        if (!(input.slice(-";".length) === ";")){
            setErrorR("; erwartet");
            return;
        }
        var s = input.replace(/\s/g, "").slice(0,-1);
        var validate = /array\[-?[0-9]*\]/.test(s);
        if (!validate) {
            setErrorR("Invalid Array selection");
            return;
        }
        setErrorR("");
        const help = s.substring(s.indexOf("[") + 1, s.indexOf("]"));
        setIndex(+help);
    }, [])

    const onMultiReadChange = useCallback((input:string) => {
        if (!(input.slice(-";".length) === ";")){
            setErrorR("; erwartet");
            return;
        }
        var s = input.replace(/\s/g, "").slice(0,-1);
        var validate = /array\[-?[0-9]*\]\[-?[0-9]*\]/.test(s);
        if (!validate) {
            setErrorR("Invalid Array selection");
            return;
        }
        setErrorR("");
        const first = s.substring(s.indexOf("[") + 1, s.indexOf("]"));
        const second = s.substring(s.indexOf("[", s.indexOf("[") + 1) + 1, s.indexOf("]", s.indexOf("]") + 1));
        setIndex(+second);
        setField(+first);
    }, [])

    const onWriteChange = useCallback((input:string) => {
        if (!(input.slice(-";".length) === ";")){
            setErrorW("; erwartet");
            return;
        }
        var s = input.replace(/\s/g, "").slice(0,-1);
        var isNull = s.slice(-"null".length) === "null";
        const key:number = +s.substring(s.indexOf("[") + 1, s.indexOf("]"));
        if (key >= array.length) {
            return;
        }
        var value:string | undefined = s.substring(s.indexOf("=") + 1, s.length);
        if (type === "string"){
            value = value.substring(1,value.length - 1);
        }
        if (isNull) {
            value = undefined;
        }
        var validate = /\[-?[0-9]\]=[0-9]/.test(s);
        if (type === "bool") {
            validate = /\[[0-9]\]=[true,false]/.test(s);
        }
        if (type === "string") {
            validate = /\[[0-9]\]="[a-z,0-9\s]"/.test(s);
        }
        if (!validate && !isNull) {
            setErrorW("Invalid Manipulation");
            return;
        }
        setIndex(key);
        setErrorW("");
        const help:Array<any> = [...array];
        help[key] = value;
        setArray(help)
    }, [array, type, setArray])

    const onMultiWriteChange = useCallback((input:string) => {
        if (!(input.slice(-";".length) === ";")){
            setErrorW("; erwartet");
            return;
        }
        var s = input.replace(/\s/g, "").slice(0,-1);
        var isNull = s.slice(-"null".length) === "null";
        const first = +s.substring(s.indexOf("[") + 1, s.indexOf("]"));
        const second = +s.substring(s.indexOf("[", s.indexOf("[") + 1) + 1, s.indexOf("]", s.indexOf("]") + 1));
        if (second >= array[first].length) {
            return;
        }
        var value:string | undefined = s.substring(s.indexOf("=") + 1, s.length);
        if (type === "string"){
            value = s.substring(0,s.length);
        }
        if (isNull) {
            value = undefined;
        }
        var validate = /\[-?[0-9]\]\[-?[0-9]\]=[0-9]/.test(s);
        if (type === "bool") {
            validate = /\[[0-9]\]\[-?[0-9]\]=[true,false]/.test(s);
        }
        if (type === "string") {
            validate = /\[[0-9]\]\[-?[0-9]\]="[a-z,0-9\s]"/.test(s);
        }
        if (!validate && !isNull) {
            setErrorW("Invalid Manipulation");
            return;
        }
        setField(first);
        setIndex(second);
        setErrorW("");
        const help:Array<any> = [...array];
        help[first][second] = value;
        setArray(help);
    }, [array, type])

    const traversCallback = useCallback((start: number, end: number, direction: number) => {
        setIndex(start);
        setStartIndex(start);
        setEndIndex(end);
        setDirection(direction);
        if (type === "string") {
            setAccumulator("");
        }
        if (type === "int") {
            setAccumulator(0);
        }
        if (type === "bool") {
            setAccumulator(array[0]);
        }
    }, [type,array])

    useEffect(() => {
        if (index !== undefined) {
            if (endIndex !== undefined) {
                if (index < 0 || index >= array.length) {
                    return;
                }
                if (index >= endIndex) {
                    setTimeout(() => setEndIndex(undefined), 5000);
                    setTimeout(() => setStartIndex(undefined), 5000);
                    setTimeout(() => setAccumulator(undefined), 5000);
                    setTimeout(() => setIndex(undefined), 5000);
                    return;
                }
                setTimeout(() => {setIndex(index+direction)}, 2000);
            }
        }
    }, [endIndex, direction,array,index])

    useEffect(() => {
        if (accumulator === undefined) {
            return;
        }
        if (accumulator === "NullPointerException") {
            return;
        }
        if (prevIndex !== index) {
            if (array[index!] === undefined) {
                setAccumulator("NullPointerException");
                setTimeout(() => setAccumulator(undefined), 5000);
            }else{
                if (type === "string") {
                    setAccumulator(accumulator+array[index!]);
                }
                if (type === "int") {
                    setAccumulator(accumulator+(+array[index!]));
                }
                if (type === "bool") {
                    setAccumulator(accumulator || array[index!]);
                }
            }
        }
    }, [accumulator, index, type])

    return (
        <div>
            {props.multi ?
                <h1>2-D Array</h1>
            :
                <h1>1-D Array</h1>
            }
            <label>Initialisierung</label>
            <p>Ein Array muss zunächst erstellt (instanziiert) werden.</p>
            <Section label={props.multi ? "[][] array = " : "[] array = "} callback={props.multi ? onMultiArrayChange : onArrayChange} placeholder={props.multi ? "{{},{}};" : "{};"} error={errorA}>
                <select onChange={(e) => setType(e.target.value)} className="typeSelect">
                    <option value="int">int</option>
                    <option value="boolean">boolean</option>
                    <option value="String">String</option>
                </select>
            </Section>
            <p>Ein Array besitzt verschiedene Attribute. Länge: {array.length} Typ: {props.multi ? type + "[]" : type}</p>
            {props.multi ? 
                <MultiArray array={array} index={index} accumulator={accumulator} field={field}/>
            :
                <Array array={array} index={index} accumulator={accumulator} travers={{end: endIndex, start:startIndex}}/>
            }
            {array.length !== 0 &&
            <div className="functions">
                <Function name="Adressierung" paragraph="Du kannst Elemente aus einem Array auslesen und Variablen zuweisen.">
                    <Section label={type + " variable = "} callback={props.multi ? onMultiReadChange : onReadChange} placeholder={props.multi ? "array[0][0];" : "array[0];"} error={errorR} />
                </Function>
                <Function name="Manipulation" paragraph="Du kannst ein Element an einer vorher bestimmten Position ändern.">
                    <Section label="array " callback={props.multi ? onMultiWriteChange : onWriteChange} placeholder={props.multi ? "[0][0] = 0;" : "[0] = 0;"} error={errorW} />
                </Function>
                {!props.multi && <Function name="Traversierung" paragraph="Du kannst Arrays durchlaufen.">
                    <Traversing callback={traversCallback} disabled={accumulator ? true : false} multi={props.multi}/>
                </Function>}
            </div>
            }
        </div>
    )
}