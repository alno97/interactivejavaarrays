import { useCallback, useState } from "react";

interface SectionProps {
    label: string;
    placeholder: string;
    callback: (value:string) => void;
    error: string;
    children?: any;
}

export function Section(props:SectionProps) {
    const [value, setValue] = useState("");
    return (
        <div>
            <div>
                {props.children}
                <label className="sectionLabel">{props.label}</label>
                <input onChange={(e) => setValue(e.target.value)} placeholder={props.placeholder}></input>
            </div>
            <div>
                <button className="execute" onClick={() => props.callback(value)}>Ausführen</button>
                {props.error && <label className="error">{props.error}</label>}
            </div>
        </div>
    )
}

interface FunctionProps {
    children: any;
    name: string;
    paragraph: string;
}

export function Function(props:FunctionProps) {
    return (
        <div className="function">
            <label>{props.name}</label>
            <p>{props.paragraph}</p>
            {props.children}
        </div>
    )
}

interface TraversProps {
    callback: (start: number, end: number, direction: number) => void;
    disabled: boolean;
    multi: boolean;
}

export function Traversing(props:TraversProps) {
    const [start, setStart] = useState(0);
    const [end, setEnd] = useState(1);
    const [direction, setDirection] = useState(1);

    const onSubmit = useCallback(() => {
        props.callback(start, end, direction)
    }, [direction, end, props, start])

    return (
        <div className="travers">
            <div>
                <label>Startindex = </label>
                <input className="counter" style={{background: "green"}} type="number" onChange={(e) => setStart(+e.target.value)} placeholder="0"/>
            </div>
            <div>
                <label>Endindex = </label>
                <input className="counter" style={{background: "red"}} type="number" onChange={(e) => setEnd(+e.target.value)} placeholder="1"/>
            </div>
            <div>
                <label>Richtung = </label>
                <select onChange={(e) => setDirection(+e.target.value)}>
                    <option value={1}>+1</option>
                    <option value={-1}>-1</option>
                </select>
            </div>
            <div>
                <button type="button" onClick={() => onSubmit()} className="execute" disabled={props.disabled}>Ausführen</button>
            </div>
        </div>
    )
}