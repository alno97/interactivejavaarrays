interface MultiArrayProps {
    array: Array<Array<any>>;
    index?: number;
    accumulator?: number;
    field?: number;
}

export function MultiArray(props:MultiArrayProps) {
    return (
        <div className="array">
            {props.field! < 0 &&
                <div className="arrayRow">
                    <label>{"array[" + props.field + "]"}</label>
                    <i className="fa fa-arrow-right left-margin" />
                    <div className="arrayError" data-tooltip={"OutOfBounds"}>
                        <i className="fa fa-bolt" />
                    </div>
                </div>
            }
            {props.array.map((array:Array<any>, index:number) => (
                <div className="arrayRow">
                    <label>{"array[" + index + "]"}</label>
                    <i className="fa fa-arrow-right left-margin" />
                    {props.field === index ?
                        <Array array={array} index={props.index}/>
                        :
                        <Array array={array}/>
                    }
                </div>
            ))}
            {props.field! >= props.array.length &&
               <div className="arrayRow">
                    <label>{"array[" + props.field + "]"}</label>
                    <i className="fa fa-arrow-right left-margin" />
                    <div className="arrayError" data-tooltip={"OutOfBounds"}>
                        <i className="fa fa-bolt" />
                    </div>
                </div>
            }
        </div>
    )
}

interface ArrayProps {
    array: Array<any>;
    index?: number;
    accumulator?: number;
    travers?: {start?: number, end?:number};
}

export function Array(props:ArrayProps) {

    return (
        <div className="arrayColumn">
            {props.index! < 0 &&
                <Item index={props.index!} value={""} className="content selected" error={true} length={props.array.length}/>
            }
            {props.array.map((value:number, index: number) => (
                <Item
                    value={value !== undefined ? value.toString() : ""} 
                    index={index} 
                    className={props.index === index ? "content selected" : "content"}
                    accumulator={props.accumulator}
                    length={props.array.length}
                    indexStyle={props.travers?.start === index ? "green" : props.travers?.end === index ? "red" : ""}
                />
            ))}
            {props.index! >= props.array.length && 
                <Item index={props.index!} value={""} className="content selected" error={true} length={props.array.length}/>
            }
        </div>
    )
}

interface ItemProps {
    index: number;
    value: string;
    className: string;
    accumulator?: number;
    error?: boolean;
    length: number;
    indexStyle?:string;
}

function Item(props:ItemProps) {
    return (
        <div className="row">
            {(props.index > props.length) && <label>...</label>}
            <div className="item">
                <div style={{color: props.indexStyle}}>{props.index}</div>
                <button className={props.className} data-tooltip={props.accumulator !== undefined ? "Akkumulator: " + props.accumulator.toString() : props.accumulator}>
                    {props.error ?
                        <div className="arrayError" data-tooltip={"OutOfBounds"}>
                            <i className="fa fa-bolt" />
                        </div>
                    :
                        <label>{props.value}</label>
                    }
                </button>
            </div>
            {props.index < -1 && <label>...</label>}
        </div>
    )
}