import './App.css';
import PageLayout from './components/layout/PageLayout';
import ArrayPage from './pages/array/ArrayPage';

export default function App() {
  return (
    <div className="App">
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
      <PageLayout />
    </div>
  );
}
