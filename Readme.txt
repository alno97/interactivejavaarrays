Git zur Verwaltung der Dateien für das Hauptseminar "Interaktive Visualisierung von Java-Arrays" am Fachgebiet Grafische Datenverarbeitung der TU Ilmenau. 

Arbeit:
Die komplette Ausarbeitung der Arbeit ist im Ordner Latex zu finden. Zur Betrachtung der fertigen Arbeit kann die PDF-Datei "hauptseminar.pdf" geöffnet werden.

Code:
Die Implementierung ist im Ordner web-app zu finden. Hier sind alle Framework Dateien enthalten, um Einblicke in den Code zu erhalten. 

Anwendung:
Die Anwendung ist im Ordner Anwendung zu finden. Um die Einzelanwendung zu betrachten kann die Datei "index.html" geöffnet werden.

Quellen:
Nicht alle Quellen sind enthalten. 
Zu einer Abwandlung des Buches zu Ullenboom: https://openbook.rheinwerk-verlag.de/javainsel/04_001.html#u4
bzw. : http://dev.usw.at/manual/java/javainsel/javainsel_03_005.htm#Rxx365java03005040000F81F03C100
Die Quelle zu Arnold ist nur teilweise enthalten. Link zum kompletten Buch: https://www.acs.ase.ro/Media/Default/documents/java/ClaudiuVinte/books/ArnoldGoslingHolmes06.pdf